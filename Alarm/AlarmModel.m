//
//  MessageModel.m
//  BetKeeper
//
//  Created by Billy on 3/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "AlarmModel.h"
#import "AppDelegate.h"

@implementation AlarmModel

@synthesize m_time;
@synthesize m_status;
@synthesize dateArray;
@synthesize m_sound_name;
@synthesize m_vibrateStatus;

- (id)initialze
{
    if(self.dateArray != nil)
        [self.dateArray release];
    
    self.dateArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 7; i++) {
        [self.dateArray addObject:@"0"];
    }
	[self reset];
	return self;
}

- (void)createModel
{
    self.dateArray = [[NSMutableArray alloc] init];
    [self reset];
}

- (void)reset
{
    self.m_time = @"";
    self.m_status = NO;
    self.m_sound_name = @"";
    m_vibrateStatus = NO;
}

- (void)setAlarm:(NSString *)_m_time Status:(BOOL)_m_status SoundName:(NSString *)_m_sound_name VibrateStatus:(BOOL)_m_vibrate_status
{
	self.m_time = _m_time;
	self.m_status = _m_status;
    self.m_sound_name = _m_sound_name;
    self.m_vibrateStatus = _m_vibrate_status;
}

- (void)addDate:(NSString *)date_str
{
    [self.dateArray addObject:date_str];
}

- (void)dealloc
{
    [self.m_time release];
    [self.dateArray release];
    [self.m_sound_name release];
}

@end
