//
//  SelectCharacterController.h
//  Alarm
//
//  Created by Billy on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCharacterController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView  *characterImgView;
@property (strong, nonatomic) IBOutlet UIButton     *selectCharacterBtn;
@property (strong, nonatomic) IBOutlet UIButton     *previousBtn;
@property (strong, nonatomic) IBOutlet UIButton     *nextBtn;
@property (strong, nonatomic) IBOutlet UIButton     *soundBtn;
@property (strong, nonatomic) IBOutlet UIButton     *backBtn;
@property (strong, nonatomic) IBOutlet UILabel      *characterLabel;
@property (strong, nonatomic) IBOutlet UILabel      *soundStatusLabel;
@property (nonatomic) int currentId;
@property (nonatomic) int character_id;

- (IBAction)selectCharacterBtnClicked:(id)sender;
- (IBAction)previousBtnClicked:(id)sender;
- (IBAction)nextBtnClicked:(id)sender;
- (IBAction)soundButtonClicked:(id)sender;
- (IBAction)backBtnClicked:(id)sender;
- (void)showCharacter:(int)characterId;

@end
