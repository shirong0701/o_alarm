//
//  AppDelegate.h
//  Alarm
//
//  Created by Billy on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainPageController.h"
#import "AddAlarmPageController.h"
#import "TimeSettingPageController.h"
#import "SelectTimePageController.h"
#import "SelectDayPageController.h"
#import "RingtonePageController.h"
#import "SelectCharacterController.h"
#import "AlarmModel.h"
#import "SoundFx.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) IBOutlet UIWindow                 *window;
@property (strong, nonatomic) IBOutlet UINavigationController   *navController;
@property (strong, nonatomic) MainPageController                *m_mainPageController;
@property (strong, nonatomic) AddAlarmPageController            *m_addAlarmPageController;
@property (strong, nonatomic) TimeSettingPageController         *m_timeSettingPageController;
@property (strong, nonatomic) SelectTimePageController          *m_selectTimePageController;
@property (strong, nonatomic) SelectDayPageController           *m_selectDayPageController;
@property (strong, nonatomic) RingtonePageController            *m_ringtinePageController;
@property (strong, nonatomic) SelectCharacterController         *m_selectCharacterController;
@property (nonatomic) UIBackgroundTaskIdentifier bgTask;

@property (strong, nonatomic) NSMutableArray    *m_alarmArray;
@property (strong, nonatomic) NSMutableArray    *m_soundArray;
@property (strong, nonatomic) NSTimer           *alarm_timer;
@property (strong, nonatomic) SoundFx*	sound;
@property (nonatomic) BOOL isEnableSound;
@property (nonatomic) int countdown;
@property (strong, nonatomic) NSMutableArray    *m_characterArray;
@property (strong, nonatomic) NSString          *m_currentCharacter;
@property (nonatomic) BOOL notificationFalg;
@property (strong, nonatomic) NSString *notifSoundStr;

- (void)initControllers;
- (void)setRootView:(id)newRootViewController;
- (void)initObjects;
- (void)executeAlarm;
- (void)startAlarmTimer;
- (void)audioPlay:(NSString *)soundName isVibrate:(BOOL)_isVibrate;
- (void)audioStop;
- (int)getFirstAlarm;
- (void)procBackground:(UIApplication *)application;
- (int)updateDay:(CFGregorianDate)data;

@end
