//
//  SelectDayPageController.m
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SelectDayPageController.h"
#import "AppDelegate.h"

@implementation SelectDayPageController

@synthesize m_tableView;
@synthesize doneBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.m_tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

// Customize the number of rows in the table view.
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Repeat";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 44)];
	
    UIButton *checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGRect frame = CGRectMake(5, 7, 30, 30);
    checkBtn.frame = frame;
    [checkBtn setTag:indexPath.row];
    
    for (int i = 0; i < 7; i++) {
        if (i == indexPath.row) {
            NSString *str = @"";
            str = [model.dateArray objectAtIndex:i];
            if ([str intValue] == 1) {
                [checkBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            }
            else
                [checkBtn setBackgroundImage:[UIImage imageNamed:@"un check.png"] forState:UIControlStateNormal];
        }
    }
    [checkBtn addTarget:self action:@selector(checkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *dayLabel =[[UILabel alloc] initWithFrame:CGRectMake(40, 4, 240, 36)];
    dayLabel.backgroundColor = [UIColor clearColor];
    dayLabel.font = [UIFont boldSystemFontOfSize:22];
    
    NSString *dayStr = @"";
    
    switch (indexPath.row) {
        case 0:
            dayStr = @"Monday";             
            break;
        case 1:
            dayStr = @"Tuesday";
            break;
        case 2:
            dayStr = @"Wednesday";
            break;
        case 3:
            dayStr = @"Thursday";
            break;
        case 4:
            dayStr = @"Friday";
            break;
        case 5:
            dayStr = @"Saturday";
            break;
        case 6:
            dayStr = @"Sunday";             
            break;
        default:
            break;
    }
    if ([model.dateArray count] == 0) {
        [checkBtn setBackgroundImage:[UIImage imageNamed:@"un check.png"] forState:UIControlStateNormal];
    }

	[dayLabel setText:dayStr];

	[cellView addSubview:dayLabel];
	[cellView addSubview:checkBtn];
    
    for (UIView *subview in cell.contentView.subviews)
		[subview removeFromSuperview];
    
	[cell.contentView addSubview:cellView];
    
	return cell;
}

- (IBAction)checkBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    UIButton *button = (UIButton *)sender;
    int row = button.tag;
    NSString *addIdStr = @"";
    addIdStr = [NSString stringWithFormat:@"%d", row];
    
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    AlarmModel *new_model = [[AlarmModel alloc] initialze];
    
    new_model.m_time = model.m_time;
    new_model.m_status = model.m_status;
    [new_model.dateArray removeAllObjects];
    
    for (NSString *str in model.dateArray) {
        [new_model.dateArray addObject:str];
    }
    
    int isChecked = 0;
    isChecked = [[new_model.dateArray objectAtIndex:row] intValue];
    
    if (isChecked == 0) {
        isChecked = 1;
    }
    else
    {
         isChecked = 0;   
    }
    
    NSString *isCheckedStr = @"";
    isCheckedStr = [NSString stringWithFormat:@"%d", isChecked];
    [new_model.dateArray replaceObjectAtIndex:row withObject:isCheckedStr];
    new_model.m_sound_name = model.m_sound_name;
    new_model.m_vibrateStatus = model.m_vibrateStatus;
    [appDelegate.m_alarmArray replaceObjectAtIndex:appDelegate.m_timeSettingPageController.arrayId withObject:new_model];
    [new_model release];
    
    [self.m_tableView reloadData];
}

- (IBAction)doneBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.m_tableView release];
    [self.doneBtn release];
}

@end
