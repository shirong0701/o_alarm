//
//  MessageModel.h
//  BetKeeper
//
//  Created by Billy on 3/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlarmModel : NSObject {
	NSString *m_time;
	BOOL m_status;
	NSMutableArray *dateArray;
    NSString *sound_name;
    BOOL m_vibrateStatus;
}

@property(strong, nonatomic) NSString       *m_time;
@property(strong, nonatomic) NSMutableArray *dateArray;
@property(nonatomic) BOOL m_status;
@property(strong, nonatomic) NSString *m_sound_name;
@property(nonatomic) BOOL m_vibrateStatus;

- (id)initialze;
- (void)reset;
- (void)setAlarm:(NSString *)_m_time Status:(BOOL)_m_status SoundName:(NSString *)_m_sound_name VibrateStatus:(BOOL)_m_vibrate_status;
- (void)addDate:(NSString *)date_str;
- (void)createModel;

@end
