//
//  SelectTimePageController.h
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectTimePageController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel  *currentTimeLabel;
@property (strong, nonatomic) IBOutlet UIButton *hourPlusBtn;
@property (strong, nonatomic) IBOutlet UIButton *minutePlusBtn;
@property (strong, nonatomic) IBOutlet UIButton *hourMinusBtn;
@property (strong, nonatomic) IBOutlet UIButton *minuteMinusBtn;
@property (strong, nonatomic) IBOutlet UIButton *hourShowBtn;
@property (strong, nonatomic) IBOutlet UIButton *minuteShowBtn;
@property (strong, nonatomic) IBOutlet UIButton *setBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (nonatomic) int hourVal;
@property (nonatomic) int minuteVal;

- (IBAction)hourPlusBtnClicked:(id)sender;
- (IBAction)minutePlusBtnClicked:(id)sender;
- (IBAction)hourMinusBtnClicked:(id)sender;
- (IBAction)minuteMinusBtnClicked:(id)sender;
- (IBAction)setBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;
- (void)showHour;
- (void)showMinute;

@end
