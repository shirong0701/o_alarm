//
//  MainPageController.h
//  Alarm
//
//  Created by Billy on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MainPageController : UIViewController<UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIButton     *selectCharacterBtn;
@property (strong, nonatomic) IBOutlet UIButton     *alarmBtn;
@property (strong, nonatomic) IBOutlet UIButton     *soundButton;
@property (strong, nonatomic) IBOutlet UILabel      *soundStatusLabel;
@property (strong, nonatomic) IBOutlet UIImageView  *CharacterImgView;
@property (strong, nonatomic) IBOutlet UILabel      *characterNameLabel;
@property (nonatomic) int character_id;
@property (nonatomic) CGPoint prevTranslation;
@property (nonatomic) int strokeCount;

- (IBAction)selectCharacterBtnClicked:(id)sender;
- (IBAction)alarmBtnClicked:(id)sender;
- (IBAction)soundBtnClicked:(id)sender;
- (void) panGestureMoveAround: (UIPanGestureRecognizer *)gesture;

@end
