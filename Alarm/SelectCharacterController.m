//
//  SelectCharacterController.m
//  Alarm
//
//  Created by Billy on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SelectCharacterController.h"
#import "AppDelegate.h"

@implementation SelectCharacterController

@synthesize characterImgView;
@synthesize selectCharacterBtn;
@synthesize previousBtn;
@synthesize nextBtn;
@synthesize soundBtn;
@synthesize backBtn;
@synthesize characterLabel;
@synthesize soundStatusLabel;
@synthesize currentId;
@synthesize character_id;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.currentId = 0;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isEnableSound == YES) {
        [self.soundBtn setBackgroundImage:[UIImage imageNamed:@"soundon.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound On";
    }
    else
    {
        [self.soundBtn setBackgroundImage:[UIImage imageNamed:@"soundoff.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound Off";
    }
    
    NSString *imgName = @"";
    imgName = [appDelegate.m_currentCharacter substringToIndex:[appDelegate.m_currentCharacter length] -  5];
    character_id = 0;
    NSMutableArray *imgArray = [[NSMutableArray alloc] initWithCapacity:22];
    for (int i = 1; i <= 22; i++) {
        NSString *tempImgName = @"";
        tempImgName = [NSString stringWithFormat:@"%@%d.png", imgName, i];
        UIImage *image = [UIImage imageNamed:tempImgName];
        if (!image) {
            continue;
        }
        tempImgName = @"";
        [imgArray addObject:image];
    }
    self.characterImgView.animationImages = imgArray;
    [imgArray release];
    
    self.characterImgView.animationDuration = 3;
    self.characterImgView.animationRepeatCount = 1;
    [self.characterImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:appDelegate.m_currentCharacter]]];

    if (currentId <= 0) {
        [self.previousBtn setEnabled:NO];
    }
    else
        [self.previousBtn setEnabled:YES];
    if (currentId >= [appDelegate.m_characterArray count] - 1) {
        [self.nextBtn setEnabled:NO];
    }
    else
        [self.nextBtn setEnabled:YES];
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
	return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.characterImgView stopAnimating];
    CGPoint pt = [[touches anyObject] locationInView:self.view];
    if ((pt.x >= 60 && pt.x <= 260) && (pt.y >= 95 && pt.y <= 225)) {
        character_id ++;
        NSString *imgName = @"";
        imgName = [appDelegate.m_characterArray objectAtIndex:self.currentId];
        imgName = [imgName substringToIndex:[imgName length] -  5];
        NSString *tempImgName = @"";
        if (character_id >= 4) {
            character_id = 0;
            tempImgName = [NSString stringWithFormat:@"%@%d.png", imgName, character_id];
        }
        else
        {
            tempImgName = [NSString stringWithFormat:@"%@%d.png", imgName, character_id];
        }
        [self.characterImgView setImage:[UIImage imageNamed:tempImgName]];
    }
}

- (IBAction)selectCharacterBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.m_currentCharacter = [appDelegate.m_characterArray objectAtIndex:self.currentId];
}

- (IBAction)previousBtnClicked:(id)sender
{
    currentId--;
    character_id = 0;
    [self showCharacter:currentId];
}

- (IBAction)nextBtnClicked:(id)sender
{
    currentId++;
    character_id = 0;
    [self showCharacter:currentId];
}

- (void)showCharacter:(int)characterId
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *characterStr = @"";
    if (characterId == 0) {
        [self.previousBtn setEnabled:NO];
    }
    else if(characterId < 0)
    {
        [self.previousBtn setEnabled:NO];
        currentId ++;
    }
    else
    {
        [self.previousBtn setEnabled:YES];
    }
    if (characterId == [appDelegate.m_characterArray count] - 1) {
        [self.nextBtn setEnabled:NO];
    }
    else if(characterId > [appDelegate.m_characterArray count] - 1)
    {
        [self.nextBtn setEnabled:NO];
        currentId --;
    }
    else
    {
        [self.nextBtn setEnabled:YES];
    }
    
    characterStr = [appDelegate.m_characterArray objectAtIndex:currentId];
    [self.characterImgView setImage:[UIImage imageNamed:characterStr]];
    NSString *imgName = @"";
    imgName = [characterStr substringToIndex:[characterStr length] -  5];
    
    NSMutableArray *imgArray = [[NSMutableArray alloc] initWithCapacity:22];
    for (int i = 1; i <= 22; i++) {
        NSString *tempImgName = @"";
        tempImgName = [NSString stringWithFormat:@"%@%d.png", imgName, i];
        UIImage *image = [UIImage imageNamed:tempImgName];
        if (!image) {
            continue;
        }
        tempImgName = @"";
        [imgArray addObject:image];
    }
    self.characterImgView.animationImages = imgArray;
    [imgArray release];
}

- (IBAction)soundButtonClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isEnableSound == YES) {
        appDelegate.isEnableSound = NO;
        [self.soundBtn setBackgroundImage:[UIImage imageNamed:@"soundoff.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound Off";
        [appDelegate audioStop];
    }
    else
    {
        appDelegate.isEnableSound = YES;
        if (appDelegate.notificationFalg == YES) {
            if (appDelegate.notifSoundStr == nil || [appDelegate.notifSoundStr length] == 0 || appDelegate.notifSoundStr == @"") {
                NSLog(@"appDelegate.notifSoundStr = nil");
            }
            
            [appDelegate audioPlay:appDelegate.notifSoundStr isVibrate:YES];
        }

        [self.soundBtn setBackgroundImage:[UIImage imageNamed:@"soundon.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound On";
    }
}

- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.characterImgView release];
    [self.selectCharacterBtn release];
    [self.previousBtn release];
    [self.nextBtn release];
    [self.soundBtn release];
    [self.backBtn release];
    [self.characterLabel release];
    [self.soundStatusLabel release];
}

@end
