//
//  AppDelegate.m
//  Alarm
//
//  Created by Billy on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize navController;
@synthesize m_mainPageController;
@synthesize m_addAlarmPageController;
@synthesize m_timeSettingPageController;
@synthesize m_selectTimePageController;
@synthesize m_selectDayPageController;
@synthesize m_ringtinePageController;
@synthesize m_selectCharacterController;
@synthesize m_alarmArray;
@synthesize m_soundArray;
@synthesize sound;
@synthesize alarm_timer;
@synthesize isEnableSound;
@synthesize countdown;
@synthesize m_characterArray;
@synthesize m_currentCharacter;
@synthesize bgTask;
@synthesize notificationFalg;
@synthesize notifSoundStr;

- (void)dealloc
{
    [_window release];
    [self.navController release];
    [self.m_mainPageController release];
    [self.m_addAlarmPageController release];
    [self.m_timeSettingPageController release];
    [self.m_selectTimePageController release];
    [self.m_selectDayPageController release];
    [self.m_ringtinePageController release];
    [self.m_selectCharacterController release];
    [self.m_alarmArray release];
    [self.m_soundArray release];
    [self.sound release];
    [self.alarm_timer release];
    [self.m_characterArray release];
    [self.m_currentCharacter release];
    [self.notifSoundStr release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.m_currentCharacter = @"character10.png";
    [self startAlarmTimer];
	[self initControllers];
    [self initObjects];
    [_window setRootViewController:self.m_mainPageController];
//    [_window addSubview:self.m_mainPageController.view];
    
//    [self setRootView:self.m_mainPageController];
    return YES;
}

- (void)startAlarmTimer
{
    self.countdown = 0;
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    NSString *str = @"";
    [formatter setDateFormat:@"ss"];
    str = [formatter stringFromDate:today];
    self.countdown = [str intValue];
    if (alarm_timer != nil) {
        if ([alarm_timer isValid]) {
            [alarm_timer invalidate];
            alarm_timer = nil;
        }
    }
    countdown++;
    alarm_timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(executeAlarm) userInfo:nil repeats:YES];
}

- (void)executeAlarm;
{   
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    NSString *day_str = @"";
    [formatter setDateFormat:@"EEE"];
    day_str = [formatter stringFromDate:today];
    
    NSString *secStr = @"";
    [formatter setDateFormat:@"ss"];
    secStr = [formatter stringFromDate:today];
    
    NSString *timeStr = @"";
    
    int count_down = [secStr intValue];

    if (count_down == 0) 
    {
        for (int i = 0; i < [self.m_alarmArray count]; i++) 
        {
            AlarmModel *model = [self.m_alarmArray objectAtIndex:i];
            if (model.m_status == YES) 
            {
                for (int j = 0; j < [model.dateArray count]; j++) 
                {
                    NSString *indexStr = @"";
                    NSString *dayStr = @"";
                    indexStr = [model.dateArray objectAtIndex:j];
                    if ([indexStr isEqualToString:@"1"]) {
                        switch (j) {
                            case 0:
                                dayStr = @"Mon";
                                break;
                            case 1:
                                dayStr = @"Tue";
                                break;
                            case 2:
                                dayStr = @"Wed";
                                break;
                            case 3:
                                dayStr = @"Thu";
                                break;
                            case 4:
                                dayStr = @"Fri";
                                break;
                            case 5:
                                dayStr = @"Sat";
                                break;
                            case 6:
                                dayStr = @"Sun";
                                break;
                            default:
                                break;
                        }
                    }
                    if ([day_str isEqualToString:dayStr]) {
                        [formatter setDateFormat:@"HH:mm"];
                        timeStr = [formatter stringFromDate:today];
                        if ([timeStr isEqualToString:model.m_time]) {
                            [self.m_ringtinePageController audioStop];
                            [self setRootView:self.m_mainPageController];
                            [self audioPlay:model.m_sound_name isVibrate:model.m_vibrateStatus];
                            
                            [alarm_timer invalidate];
                            alarm_timer = nil;
                            break;
                        }
                    }
                }
            }
        }   
    }
}

- (void)audioPlay:(NSString *)soundName isVibrate:(BOOL)_isVibrate
{
	NSBundle *mainBundle = [NSBundle mainBundle];
    if (self.sound) {
        self.sound = nil;
    }

    [self.sound releaseSound];
	self.sound = [[SoundFx alloc] initWithContentsOfFile:[mainBundle pathForResource:soundName ofType:@"mp3"]];
    if (_isVibrate == YES) {
        [self.sound playVibrate];
    }
    [self.sound play];
}

- (void)audioStop
{
	[self.sound stop];	
}

- (void)setRootView:(id)newRootViewController
{
	navController = [[UINavigationController alloc] initWithRootViewController:newRootViewController];
	navController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [navController setNavigationBarHidden:YES];
	[self.window addSubview:navController.view];
	[self.window makeKeyAndVisible];
}

- (void)initControllers
{
	m_mainPageController = [[MainPageController alloc] initWithNibName:@"MainPageController" bundle:nil];
    m_addAlarmPageController = [[AddAlarmPageController alloc] initWithNibName:@"AddAlarmPageController" bundle:nil];
    m_timeSettingPageController = [[TimeSettingPageController alloc] initWithNibName:@"TimeSettingPageController" bundle:nil];
    m_selectTimePageController = [[SelectTimePageController alloc] initWithNibName:@"SelectTimePageController" bundle:nil];
    m_selectDayPageController = [[SelectDayPageController alloc] initWithNibName:@"SelectDayPageController" bundle:nil];
    m_ringtinePageController = [[RingtonePageController alloc] initWithNibName:@"RingtonePageController" bundle:nil];
    m_selectCharacterController = [[SelectCharacterController alloc] initWithNibName:@"SelectCharacterController" bundle:nil];
}

- (void)initObjects
{
    self.m_alarmArray = [[NSMutableArray alloc] init];
    self.m_soundArray = [[NSMutableArray alloc] init];
    self.m_characterArray = [[NSMutableArray alloc] init];
    [self.m_soundArray addObject:@"dinky"];
    [self.m_soundArray addObject:@"inky"];
    [self.m_soundArray addObject:@"pinky"];
    [self.m_characterArray addObject:@"character10.png"];
    [self.m_characterArray addObject:@"character20.png"];
    [self.m_characterArray addObject:@"character30.png"];
    self.isEnableSound = YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [self audioStop];
    [self.m_ringtinePageController audioStop];
    if (alarm_timer != nil) {
        if ([alarm_timer isValid]) {
            [alarm_timer invalidate];
            alarm_timer = nil;
        }
    }
    NSAssert(self->bgTask == UIBackgroundTaskInvalid, nil);
    
    bgTask = [application beginBackgroundTaskWithExpirationHandler: ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [application endBackgroundTask:self->bgTask];
            self.bgTask = UIBackgroundTaskInvalid;
        });
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
//        while ([application backgroundTimeRemaining] > 1.0) {
//            NSString *friend = @"test";
//            if (friend) {
//                UILocalNotification *localNotif = [[UILocalNotification alloc] init];
//                if (localNotif) {
//                    localNotif.alertBody = [NSString stringWithFormat:NSLocalizedString(@"%@has a message for you", nil), friend];
//                    localNotif.alertAction = NSLocalizedString(@"Read Msg", nil);
//                    localNotif.soundName = @"inky.mp3";
//                    localNotif.applicationIconBadgeNumber = 1;
////                    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:@"Your Background Task works",  nil
//                    [application presentLocalNotificationNow:localNotif];
//                    [localNotif release];
//                    break;
//                }
//            }
//        }
        
        [self procBackground:application];
        [application endBackgroundTask:self->bgTask];
        self->bgTask = UIBackgroundTaskInvalid;
    });
}

- (int)getFirstAlarm
{
    int dist = 0;
    int alarmId = 0;
    int nCount = [self.m_alarmArray count];
    
    CFAbsoluteTime at = CFAbsoluteTimeGetCurrent();
    CFTimeZoneRef timeZone = CFTimeZoneCopySystem();
    CFGregorianDate data = CFAbsoluteTimeGetGregorianDate(at, timeZone);
    CFRelease(timeZone);
    
    int nCurDist = 0;
    for(int i = 0; i < nCount; i++) 
    {
        AlarmModel* model = [self.m_alarmArray objectAtIndex:i];
        if(model.m_status == NO)
            continue;
        
        NSString *rangeStr = @"";
        
        rangeStr = [[model.m_time substringToIndex:2] substringFromIndex:0];
        int nOffsetHour = [rangeStr intValue] - data.hour;
        
        rangeStr = [[model.m_time substringToIndex:5] substringFromIndex:3];
        int nOffsetMinute = [rangeStr intValue] - data.minute;

        nCurDist = nOffsetHour * 60 + nOffsetMinute;
        
        if (nCurDist < 0) {
            nCurDist += 1440;
        }
        
        if (dist == 0) {
            dist = nCurDist;
            alarmId = i;
        } else if (dist > nCurDist) {
            dist = nCurDist;
            alarmId = i;
        }
    }
    
    return alarmId;
}

- (void)procBackground:(UIApplication *)application
{
    int alarmId = [self getFirstAlarm];
    CFAbsoluteTime at = CFAbsoluteTimeGetCurrent();
    CFTimeZoneRef timeZone = CFTimeZoneCopySystem();
    CFGregorianDate data = CFAbsoluteTimeGetGregorianDate(at, timeZone);
    CFRelease(timeZone);
    int weekDay = [self updateDay:data];
    int nSecond = (int)data.second;
    
    BOOL weekDay_flag = NO;
    
    AlarmModel* model = [self.m_alarmArray objectAtIndex:alarmId];
    
    for (int i = 0; i < [model.dateArray count]; i++) {
        if (weekDay == i) {
            NSString *dayStr = @"";
            dayStr = [model.dateArray objectAtIndex:i];
            if ([dayStr isEqualToString:@"0"]) {
                weekDay_flag = NO;
                continue;
            }
            else
            {
                weekDay_flag = YES;
                break;
            }
        }
    }
    
    if (weekDay_flag == YES) {
        NSString *rangeStr = @"";
        
        rangeStr = [[model.m_time substringToIndex:2] substringFromIndex:0];
        int nOffsetHour = [rangeStr intValue] - data.hour;
        
        rangeStr = [[model.m_time substringToIndex:5] substringFromIndex:3];
        int nOffsetMinute = [rangeStr intValue] - data.minute;
        
        if (nOffsetHour < 0) {
            nOffsetHour += 24;
        }
        
        Class cls = NSClassFromString(@"UILocalNotification");
        if (cls != nil) {
            NSDate* current = [NSDate date];
            UILocalNotification *notif = [[cls alloc] init];
            notif.timeZone = [NSTimeZone defaultTimeZone];
            notif.fireDate = [current dateByAddingTimeInterval:nOffsetHour * 3600 + nOffsetMinute * 60 - nSecond];
            notif.alertBody = @"";
            notif.alertAction = @"Open Alarm";
            
            self.notifSoundStr = model.m_sound_name;
            NSString* str_sound = [NSString stringWithFormat:@"%@.%@", model.m_sound_name, @"mp3"];
            
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            NSLog(@"current sound name = %@", model.m_sound_name);
            [defaults setObject:model.m_sound_name forKey:@"currentsound"];
            [defaults synchronize];
            
            notif.soundName = [str_sound copy];
            
            NSLog(@"notif.soundName = %@", notif.soundName);
            [[UIApplication sharedApplication] scheduleLocalNotification:notif];
            [notif release];
        }
    }
}

- (int)updateDay:(CFGregorianDate)data {
	NSDateComponents *comps = [[NSDateComponents alloc] init];
	[comps setDay:data.day];
	[comps setMonth:data.month];
	[comps setYear:data.year];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDate *date = [gregorian dateFromComponents:comps];
	[comps release];
	NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
	int nWeekday = [weekdayComponents weekday]; //1:Sunday
    switch (nWeekday) {
        case 1:
            nWeekday = 6;
            break;
        case 2:
            nWeekday = 0;
            break;
        case 3:
            nWeekday = 1;
            break;
        case 4:
            nWeekday = 2;
            break;
        case 5:
            nWeekday = 3;
            break;
        case 6:
            nWeekday = 4;
            break;
        case 7:
            nWeekday = 5;
            break;
        default:
            break;
    }
	[gregorian release];
    return nWeekday;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self startAlarmTimer];
    bgTask = UIApplicationStateActive;
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {

	UIApplicationState state = [application applicationState];

	if (state == UIApplicationStateInactive) {
        notificationFalg = YES;
        [self setRootView:self.m_mainPageController];
        NSString *soundStr = @"";
        soundStr = [notification.soundName substringToIndex:[notification.soundName length] - 4];
        [self audioPlay:soundStr isVibrate:YES];
	}
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
