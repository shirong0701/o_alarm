//
//  TimeSettingPageController.h
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeSettingPageController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView  *m_tableView;
@property (strong, nonatomic) IBOutlet UIButton     *checkBtn;
@property (strong, nonatomic) IBOutlet UIButton     *doneBtn;
@property (strong, nonatomic) IBOutlet UIButton     *revertBtn;
@property (strong, nonatomic) IBOutlet UIButton     *deleteBtn;
@property (nonatomic) int arrayId;

- (IBAction)checkBtnClicked:(id)sender;
- (IBAction)doneBtnClicked:(id)sender;
- (IBAction)revertBtnClicked:(id)sender;
- (IBAction)deleteBtnClicked:(id)sender;

@end
