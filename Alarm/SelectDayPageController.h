//
//  SelectDayPageController.h
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectDayPageController : UIViewController

@property(strong, nonatomic) IBOutlet UITableView   *m_tableView;
@property(strong, nonatomic) IBOutlet UIButton      *doneBtn;

- (IBAction)checkBtnClicked:(id)sender;
- (IBAction)doneBtnClicked:(id)sender;

@end
