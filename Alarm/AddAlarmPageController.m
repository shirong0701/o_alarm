//
//  AddAlarmPageController.m
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddAlarmPageController.h"
#import "AppDelegate.h"

@implementation AddAlarmPageController

@synthesize addAlarmBtn;
@synthesize m_tableView;
@synthesize timeLabel;
@synthesize timer;
@synthesize timeStr;
@synthesize isAddAlarmPage;
@synthesize clockBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(showTime) userInfo:nil repeats:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.m_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{

}

- (void)showTime;
{
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"hh:mm a"];
    NSString *str = [formatter stringFromDate:today];
    self.timeLabel.text = str;
    [formatter setDateFormat:@"HH:mm"];
    self.timeStr = [formatter stringFromDate:today];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	return [appDelegate.m_alarmArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 80;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UISwitch *switchView = NULL;
    
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
	cell.backgroundColor = [UIColor clearColor];
    
    switchView = [[UISwitch alloc] initWithFrame: CGRectMake(220.0f, 15.0f, 100.0f, 28.0f)];
    [switchView setTag:indexPath.row];
    [switchView setOn:model.m_status];
    [switchView addTarget:self action:@selector(changeStatus:) forControlEvents:UIControlEventValueChanged];
    
    [cell addSubview:switchView];
    [switchView release];
    
    cell.textLabel.text = model.m_time;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:24.0];

    NSString *repeatStr = @"";
    NSString *dayStr = @"";
    for (int i = 0; i < 7; i++) 
    {
        NSString *itemStr = @"";
        dayStr = @"";
        switch (i) 
        {
            case 0:
                itemStr = [model.dateArray objectAtIndex:i];
                if ([itemStr isEqualToString:@"1"]) {
                    dayStr = @"Mon, ";
                }
                break;
            case 1:
                itemStr = [model.dateArray objectAtIndex:i];
                if ([itemStr isEqualToString:@"1"]) {
                    dayStr = @"Tue, ";
                }
                break;
            case 2:
                itemStr = [model.dateArray objectAtIndex:i];
                if ([itemStr isEqualToString:@"1"]) {
                    dayStr = @"Wed, ";
                }
                break;
            case 3:
                itemStr = [model.dateArray objectAtIndex:i];
                if ([itemStr isEqualToString:@"1"]) {
                    dayStr = @"Thu, ";
                }
                break;
            case 4:
                itemStr = [model.dateArray objectAtIndex:i];
                if ([itemStr isEqualToString:@"1"]) {
                    dayStr = @"Fri, ";
                }
                break;
            case 5:
                itemStr = [model.dateArray objectAtIndex:i];
                if ([itemStr isEqualToString:@"1"]) {
                    dayStr = @"Sat, ";
                }
                break;
            case 6:
                itemStr = [model.dateArray objectAtIndex:i];
                if ([itemStr isEqualToString:@"1"]) {
                    dayStr = @"Sun, ";
                }
                break;
            default:
                break;
        }
        repeatStr = [NSString stringWithFormat:@"%@%@", repeatStr, dayStr];
    }
    if ([repeatStr isEqualToString:@""]) {
        cell.detailTextLabel.text = @"Never";
    }
    else
    {
        repeatStr = [repeatStr substringToIndex:[repeatStr length] - 2];
        cell.detailTextLabel.text = repeatStr;
    }

    cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
    cell.detailTextLabel.numberOfLines = 2;

    cell.imageView.image = [UIImage imageNamed:@"clock.png"];

	return cell;
}

- (void)changeStatus:(UISwitch *)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UISwitch *switchView = (UISwitch *)sender;
    int row = switchView.tag;
    
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:row];
    if (switchView.isOn == YES) {
        model.m_status = YES;
        [appDelegate.m_alarmArray replaceObjectAtIndex:row withObject:model];
    }
    else
    {
        model.m_status = NO;
        [appDelegate.m_alarmArray replaceObjectAtIndex:row withObject:model];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	AppDelegate * appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
	
    appDelegate.m_timeSettingPageController.arrayId = indexPath.row;
    [self.navigationController pushViewController:appDelegate.m_timeSettingPageController animated:YES];
}

- (IBAction)clockBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addAddAlarmBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [[AlarmModel alloc] initialze];
    [model setAlarm:self.timeStr Status:YES SoundName:@"inky" VibrateStatus:YES];
    [appDelegate.m_alarmArray addObject:model];
    model = nil;
    [model release];
    appDelegate.m_timeSettingPageController.arrayId = [appDelegate.m_alarmArray count] - 1;
    self.isAddAlarmPage = YES;
    [self.navigationController pushViewController:appDelegate.m_timeSettingPageController animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.addAlarmBtn release];
    [self.m_tableView release];
    [self.timeLabel release];
    [self.timer release];
    [self.timeStr release];
    [self.clockBtn release];
    [super dealloc];
}

@end
