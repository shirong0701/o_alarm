//
//  RingtonePageController.h
//  Alarm
//
//  Created by Billy on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RingtonePageController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView  *m_tableView;
@property (strong, nonatomic) IBOutlet UIButton     *okBtn;
@property (strong, nonatomic) IBOutlet UIButton     *cancelBtn;
@property (nonatomic) int selectedIndex;

- (IBAction)okBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;
- (IBAction)checkBtnClicked:(id)sender;
- (void)audioPlay:(int)soundId;
- (void)audioStop;

@end
