//
//  SoundFx.m
//
//  Created by Billy on 2011 03 011
//  Copyright OSD Center. All rights reserved.
//

#import "SoundFx.h"

@implementation SoundFx
+ (id)soundFxWithContentsOfFile:(NSString *)aPath {
    if (aPath) {
        return [[[SoundFx alloc] initWithContentsOfFile:aPath] autorelease];
    }
    return nil;
}

- (id)initWithContentsOfFile:(NSString *)path {
    self = [super init];
    
    if (self != nil) {
        NSURL *aFileURL = [NSURL fileURLWithPath:path isDirectory:NO];

        
        if (aFileURL != nil)  {
            SystemSoundID aSoundID;
            OSStatus error = AudioServicesCreateSystemSoundID((CFURLRef)aFileURL, &aSoundID);
            
            
            if (error == kAudioServicesNoError) { // success
                _soundID = aSoundID;
            } else {
//                NSLog(@"Error %d loading sound at path: %@", error, path);
                [self release], self = nil;
            }
        } else {
            NSLog(@"NSURL is nil for path: %@", path);
            [self release], self = nil;
        }
    }
    return self;
}

- (void)dealloc {
    AudioServicesDisposeSystemSoundID(_soundID);
    [super dealloc];
}

- (void)play {
    AudioServicesPlaySystemSound(_soundID);
    [self checkCompletion];
}

- (void)playVibrate
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

- (void)stop {
    AudioServicesDisposeSystemSoundID(_soundID);
}

- (void)releaseSound
{
    AudioServicesRemoveSystemSoundCompletion(_soundID);
    [self stop];
//    [self stop];
}

- (void)checkCompletion
{
    AudioServicesAddSystemSoundCompletion(_soundID, NULL, NULL, endSound, NULL); 
}

void endSound (SystemSoundID ssId, void *clientData)
{
    AudioServicesPlaySystemSound(ssId);
//    [self checkCompletion];
}

- (UInt32*)getSize
{
    AudioSessionGetPropertySize(_soundID, size);
    return size;
    
}


@end