//
//  SoundFx.h
//  OneToNine
//
//  Created by Billy on 2011 03 011
//  Copyright OSD Center. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>

@interface SoundFx : NSObject {
    SystemSoundID _soundID;
    UInt32*     size;
}

+ (id)soundFxWithContentsOfFile:(NSString *)aPath;
- (id)initWithContentsOfFile:(NSString *)path;
- (void)play;
- (void)stop;
- (UInt32*)getSize;
- (void)checkCompletion;
void endSound (SystemSoundID ssId, void *clientData);
- (void)releaseSound;
- (void)playVibrate;

@end
