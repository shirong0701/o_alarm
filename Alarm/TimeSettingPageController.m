//
//  TimeSettingPageController.m
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TimeSettingPageController.h"
#import "AppDelegate.h"

@implementation TimeSettingPageController

@synthesize m_tableView;
@synthesize checkBtn;
@synthesize doneBtn;
@synthesize revertBtn;
@synthesize deleteBtn;
@synthesize arrayId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrayId = 0;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    if (model.m_vibrateStatus == YES) {
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    }
    else
    {
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"un check.png"] forState:UIControlStateNormal];
    }
    [self.m_tableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 80;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
	
	cell.backgroundColor = [UIColor clearColor];
	cell.imageView.backgroundColor = [UIColor clearColor];
    
    UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(80, 0, 220, 80)];
    
	UILabel *headLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 5, 220, 20)];
    UILabel *contentLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 27, 220, 50)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
	headLabel.backgroundColor = [UIColor clearColor];
    contentLabel.backgroundColor = [UIColor clearColor];
    
    headLabel.font = [UIFont systemFontOfSize:12.0];
    contentLabel.font = [UIFont boldSystemFontOfSize:22];
    headLabel.textColor = [UIColor grayColor];
    contentLabel.textColor = [UIColor blackColor];
    contentLabel.numberOfLines = 3;
    
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:self.arrayId];
    
    switch (indexPath.row) {
        case 0:
            headLabel.text = @"Time";
            contentLabel.text = model.m_time;
            break;
        case 1:
            headLabel.text = @"Repeat";
            NSString *repeatStr = @"";
            NSString *dayStr = @"";
            for (int i = 0; i < 7; i++) 
            {
                NSString *itemStr = @"";
                dayStr = @"";
                switch (i) 
                {
                    case 0:
                        itemStr = [model.dateArray objectAtIndex:i];
                        if ([itemStr isEqualToString:@"1"]) {
                            dayStr = @"Mon, ";
                        }
                        break;
                    case 1:
                        itemStr = [model.dateArray objectAtIndex:i];
                        if ([itemStr isEqualToString:@"1"]) {
                            dayStr = @"Tue, ";
                        }
                        break;
                    case 2:
                        itemStr = [model.dateArray objectAtIndex:i];
                        if ([itemStr isEqualToString:@"1"]) {
                            dayStr = @"Wed, ";
                        }
                        break;
                    case 3:
                        itemStr = [model.dateArray objectAtIndex:i];
                        if ([itemStr isEqualToString:@"1"]) {
                            dayStr = @"Thu, ";
                        }
                        break;
                    case 4:
                        itemStr = [model.dateArray objectAtIndex:i];
                        if ([itemStr isEqualToString:@"1"]) {
                            dayStr = @"Fri, ";
                        }
                        break;
                    case 5:
                        itemStr = [model.dateArray objectAtIndex:i];
                        if ([itemStr isEqualToString:@"1"]) {
                            dayStr = @"Sat, ";
                        }
                        break;
                    case 6:
                        itemStr = [model.dateArray objectAtIndex:i];
                        if ([itemStr isEqualToString:@"1"]) {
                            dayStr = @"Sun, ";
                        }
                        break;
                    default:
                        break;
                }
                repeatStr = [NSString stringWithFormat:@"%@%@", repeatStr, dayStr];
            }
            if ([repeatStr isEqualToString:@""]) {
                contentLabel.text = @"Never";
            }
            else
            {
                repeatStr = [repeatStr substringToIndex:[repeatStr length] - 2];
                contentLabel.text = repeatStr;
            }
            break;
        case 2:
            headLabel.text = @"Ringtone";
            if (appDelegate.m_addAlarmPageController.isAddAlarmPage == YES) {
                contentLabel.text = @"Default";
            }
            else
            {
                contentLabel.text = model.m_sound_name;
            }
            break;
        default:
            break;
    }
    
    [cellView addSubview:headLabel];
    [cellView addSubview:contentLabel];
    
    for (UIView *subview in cell.contentView.subviews)
		[subview removeFromSuperview];
    
	[cell.contentView addSubview:cellView];
    
    cell.imageView.image = [UIImage imageNamed:@"clock.png"];
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
	
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	AppDelegate * appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
	
	int row = indexPath.row;
	
	if (row == 0) {
		
		[self.navigationController pushViewController:appDelegate.m_selectTimePageController animated:YES];
	}
	else if(row == 1){
		[self.navigationController pushViewController:appDelegate.m_selectDayPageController animated:YES];
	}
	else{
		[self.navigationController pushViewController:appDelegate.m_ringtinePageController animated:YES];
	}
}


- (IBAction)checkBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    if (model.m_vibrateStatus == NO) {
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
        AlarmModel *new_model = [[AlarmModel alloc] initialze];
        
        new_model.m_time = model.m_time;
        new_model.m_status = model.m_status;
        [new_model.dateArray removeAllObjects];
        
        for (NSString *str in model.dateArray) {
            [new_model.dateArray addObject:str];
        }
        
        new_model.m_sound_name = model.m_sound_name;
        new_model.m_vibrateStatus = YES;
        
        [appDelegate.m_alarmArray replaceObjectAtIndex:appDelegate.m_timeSettingPageController.arrayId withObject:new_model];
        
        [new_model release];
        return;
    }
    else
    {
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"un check.png"] forState:UIControlStateNormal];
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
        AlarmModel *new_model = [[AlarmModel alloc] initialze];
        
        new_model.m_time = model.m_time;
        new_model.m_status = model.m_status;
        [new_model.dateArray removeAllObjects];
        
        for (NSString *str in model.dateArray) {
            [new_model.dateArray addObject:str];
        }
        
        new_model.m_sound_name = model.m_sound_name;
        new_model.m_vibrateStatus = NO;
        [appDelegate.m_alarmArray replaceObjectAtIndex:appDelegate.m_timeSettingPageController.arrayId withObject:new_model];
        
        [new_model release];
        return;
    }
}

- (IBAction)doneBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)revertBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    AlarmModel *new_model = [[AlarmModel alloc] initialze];
    
    new_model.m_time = model.m_time;
    new_model.m_status = model.m_status;
    [new_model.dateArray removeAllObjects];
    
    for (int i = 0; i < 7; i++) {
        [new_model.dateArray addObject:@"0"];
    }
    
    appDelegate.m_addAlarmPageController.isAddAlarmPage = YES;
    new_model.m_sound_name = @"inky";
    [appDelegate.m_alarmArray replaceObjectAtIndex:appDelegate.m_timeSettingPageController.arrayId withObject:new_model];
    
    [new_model release];
    [self.m_tableView reloadData];
}

- (IBAction)deleteBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.m_alarmArray removeObjectAtIndex:self.arrayId];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.m_tableView release];
    [self.checkBtn release];
    [self.doneBtn release];
    [self.revertBtn release];
    [self.deleteBtn release];
    [super dealloc];
}

@end
