//
//  RingtonePageController.m
//  Alarm
//
//  Created by Billy on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RingtonePageController.h"
#import "AppDelegate.h"

@implementation RingtonePageController

@synthesize m_tableView;
@synthesize okBtn;
@synthesize cancelBtn;
@synthesize selectedIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedIndex = -1;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    [self audioStop];
    self.selectedIndex = [appDelegate.m_soundArray indexOfObject:model.m_sound_name];
    [self.m_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.sound stop];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Ringtones";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate.m_soundArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
	UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 44)];
	
    UIButton *checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGRect frame = CGRectMake(5, 7, 30, 30);
    checkBtn.frame = frame;
    [checkBtn setTag:indexPath.row];
    
    if (self.selectedIndex == indexPath.row) {
        [checkBtn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    }
    else
    {
        [checkBtn setBackgroundImage:[UIImage imageNamed:@"un check.png"] forState:UIControlStateNormal];
    }
    
    [checkBtn addTarget:self action:@selector(checkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *dayLabel =[[UILabel alloc] initWithFrame:CGRectMake(40, 4, 240, 36)];
    dayLabel.backgroundColor = [UIColor clearColor];
    dayLabel.font = [UIFont boldSystemFontOfSize:22];
    
    for (int i = 0; i < [appDelegate.m_soundArray count]; i++) {
        if (indexPath.row == i) {
            dayLabel.text = [appDelegate.m_soundArray objectAtIndex:i];
        }
    }
    
	[cellView addSubview:dayLabel];
	[cellView addSubview:checkBtn];
    
    for (UIView *subview in cell.contentView.subviews)
		[subview removeFromSuperview];
    
	[cell.contentView addSubview:cellView];
    
	return cell;
}

- (IBAction)checkBtnClicked:(id)sender
{  
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self audioStop];
    UIButton *button = (UIButton *)sender;
    self.selectedIndex = button.tag;
    [self.m_tableView reloadData];
    [self audioPlay:self.selectedIndex];
    appDelegate.m_addAlarmPageController.isAddAlarmPage = NO;
}

- (void)audioPlay:(int)soundId
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
	NSBundle *mainBundle = [NSBundle mainBundle];
    if (appDelegate.sound) {
        appDelegate.sound = nil;
    }
    NSString *soundStr = @"";
    soundStr = [appDelegate.m_soundArray objectAtIndex:soundId];
	appDelegate.sound = [[SoundFx alloc] initWithContentsOfFile:[mainBundle pathForResource:soundStr ofType:@"mp3"]];
    [appDelegate.sound play];
}

- (void)audioStop
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate.sound stop];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self audioStop];
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
	self.selectedIndex = indexPath.row;
    [self audioPlay:self.selectedIndex];
    appDelegate.m_addAlarmPageController.isAddAlarmPage = NO;
    [self.m_tableView reloadData];
}

- (IBAction)okBtnClicked:(id)sender
{
    [self audioStop];
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    AlarmModel *new_model = [[AlarmModel alloc] initialze];
    
    new_model.m_time = model.m_time;
    new_model.m_status = model.m_status;
    [new_model.dateArray removeAllObjects];
    
    for (NSString *str in model.dateArray) {
        [new_model.dateArray addObject:str];
    }

    new_model.m_sound_name = [appDelegate.m_soundArray objectAtIndex:self.selectedIndex];
    new_model.m_vibrateStatus = model.m_vibrateStatus;
    [appDelegate.m_alarmArray replaceObjectAtIndex:appDelegate.m_timeSettingPageController.arrayId withObject:new_model];
    
    [new_model release];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelBtnClicked:(id)sender
{
    [self audioStop];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.m_tableView release];
    [self.okBtn release];
    [self.cancelBtn release];
}

@end
