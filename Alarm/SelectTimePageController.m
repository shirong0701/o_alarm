//
//  SelectTimePageController.m
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SelectTimePageController.h"
#import "AppDelegate.h"

@implementation SelectTimePageController

@synthesize currentTimeLabel;
@synthesize hourPlusBtn;
@synthesize minutePlusBtn;
@synthesize hourMinusBtn;
@synthesize minuteMinusBtn;
@synthesize hourShowBtn;
@synthesize minuteShowBtn;
@synthesize setBtn;
@synthesize cancelBtn;
@synthesize hourVal;
@synthesize minuteVal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    self.currentTimeLabel.text = model.m_time;
    
    NSString *rangeStr = @"";
    
    rangeStr = [[model.m_time substringToIndex:2] substringFromIndex:0];
    [self.hourShowBtn setTitle:rangeStr forState:UIControlStateNormal];
    hourVal = [rangeStr intValue];
    
    rangeStr = [[model.m_time substringToIndex:5] substringFromIndex:3];
    [self.minuteShowBtn setTitle:rangeStr forState:UIControlStateNormal];
    minuteVal = [rangeStr intValue];
}

- (IBAction)hourPlusBtnClicked:(id)sender
{
    if (hourVal < 0) {
        hourVal = 23;
        [self showHour];
        return;
    }
    if (hourVal >= 23) {
        hourVal = 0;
       [self showHour];
        return;
    }
    hourVal ++;
    [self showHour];
}

- (IBAction)minutePlusBtnClicked:(id)sender
{
    if (minuteVal < 0) {
        minuteVal = 59;
        [self showMinute];
        return;
    }
    if (minuteVal >= 59) {
        minuteVal = 0;
        [self showMinute];
        return;
    }
    minuteVal ++;
    [self showMinute];
}

- (IBAction)hourMinusBtnClicked:(id)sender
{
    if (hourVal <= 0) {
        hourVal = 23;
        [self showHour];
        return;
    }
    if (hourVal > 23) {
        hourVal = 0;
        [self showHour];
        return;
    }
    hourVal --;
    [self showHour];
}

- (IBAction)minuteMinusBtnClicked:(id)sender
{
    if (minuteVal <= 0) {
        minuteVal = 59;
        [self showMinute];
        return;
    }
    if (minuteVal > 59) {
        minuteVal = 0;
        [self showMinute];
        return;
    }
    minuteVal --;
    [self showMinute];
}

- (IBAction)setBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    AlarmModel *model = [appDelegate.m_alarmArray objectAtIndex:appDelegate.m_timeSettingPageController.arrayId];
    
    NSString *tmpHourStr = @"";
    NSString *tmpMinuteStr = @"";
    if (hourVal >= 0 && hourVal <= 9) {
        tmpHourStr = [NSString stringWithFormat:@"0%d", hourVal];
    }
    else
        tmpHourStr = [NSString stringWithFormat:@"%d", hourVal];
    
    if (minuteVal >= 0 && minuteVal <= 9) {
        tmpMinuteStr = [NSString stringWithFormat:@"0%d", minuteVal];
    }
    else
        tmpMinuteStr = [NSString stringWithFormat:@"%d", minuteVal];
    
    model.m_time = [NSString stringWithFormat:@"%@:%@", tmpHourStr, tmpMinuteStr];
    
    [appDelegate.m_alarmArray replaceObjectAtIndex:appDelegate.m_timeSettingPageController.arrayId withObject:model];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showHour
{
    if (hourVal >= 0 && hourVal <= 9) {
        [self.hourShowBtn setTitle:[NSString stringWithFormat:@"0%d", hourVal] forState:UIControlStateNormal];
    }
    else
        [self.hourShowBtn setTitle:[NSString stringWithFormat:@"%d", hourVal] forState:UIControlStateNormal];
}

- (void)showMinute
{
    if (minuteVal >= 0 && minuteVal <= 9) {
        [self.minuteShowBtn setTitle:[NSString stringWithFormat:@"0%d", minuteVal] forState:UIControlStateNormal];
    }
    else
        [self.minuteShowBtn setTitle:[NSString stringWithFormat:@"%d", minuteVal] forState:UIControlStateNormal];
}

- (IBAction)cancelBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    [self.currentTimeLabel release];
    [self.hourPlusBtn release];
    [self.minutePlusBtn release];
    [self.hourMinusBtn release];
    [self.minuteMinusBtn release];
    [self.hourShowBtn release];
    [self.minuteShowBtn release];
    [self.setBtn release];
    [self.cancelBtn release];

    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
