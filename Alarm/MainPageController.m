//
//  MainPageController.m
//  Alarm
//
//  Created by Billy on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainPageController.h"
#import "AppDelegate.h"

@implementation MainPageController

@synthesize selectCharacterBtn;
@synthesize alarmBtn;
@synthesize soundButton;
@synthesize soundStatusLabel;
@synthesize CharacterImgView;
@synthesize characterNameLabel;
@synthesize character_id;
@synthesize prevTranslation;
@synthesize strokeCount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIPanGestureRecognizer *panGesture = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureMoveAround:)] autorelease];
    [panGesture setMaximumNumberOfTouches:2];
    [panGesture setDelegate:self];
    [self.CharacterImgView addGestureRecognizer:panGesture];
    // Do any additional setup after loading the view from its nib.
}

- (void) panGestureMoveAround: (UIPanGestureRecognizer *)gesture
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIView *piece = [gesture view];
    
    if ([gesture state] == UIGestureRecognizerStateEnded) {
        strokeCount++;
        character_id ++;
        NSString *imgName = @"";
        imgName = [appDelegate.m_currentCharacter substringToIndex:[appDelegate.m_currentCharacter length] -  5];
        NSString *tempImgName = @"";
        if (character_id >= 4) {
            character_id = 0;
            tempImgName = [NSString stringWithFormat:@"%@%d.png", imgName, character_id];
        }
        else
        {
            tempImgName = [NSString stringWithFormat:@"%@%d.png", imgName, character_id];
        }
        [self.CharacterImgView setImage:[UIImage imageNamed:tempImgName]];

    }
    if (strokeCount == 4) {
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.m_ringtinePageController audioStop];
        [appDelegate audioStop];
        [appDelegate startAlarmTimer];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    prevTranslation = CGPointZero; 
    strokeCount = 0;
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isEnableSound == YES) {
        [self.soundButton setBackgroundImage:[UIImage imageNamed:@"soundon.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound On";
    }
    else
    {
        [self.soundButton setBackgroundImage:[UIImage imageNamed:@"soundoff.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound Off";
    }
    [self.CharacterImgView setImage:[UIImage imageNamed:appDelegate.m_currentCharacter]];
    character_id = 0;
    NSString *imgName = @"";
    imgName = [appDelegate.m_currentCharacter substringToIndex:[appDelegate.m_currentCharacter length] -  5];
    
    self.characterNameLabel.text = imgName;
    NSMutableArray *imgArray = [[NSMutableArray alloc] initWithCapacity:22];
    for (int i = 1; i <= 22; i++) {
        NSString *tempImgName = @"";
        tempImgName = [NSString stringWithFormat:@"%@%d.png", imgName, i];
        UIImage *image = [UIImage imageNamed:tempImgName];
        if (!image) {
            continue;
        }
        tempImgName = @"";
        [imgArray addObject:image];
    }
    self.CharacterImgView.animationImages = imgArray;
    [imgArray release];
    
    self.CharacterImgView.animationDuration = 2;
    self.CharacterImgView.animationRepeatCount = 1;
}

- (void)viewWillDisappear:(BOOL)animated
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.notificationFalg = NO;    
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
	return YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)selectCharacterBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self.navigationController pushViewController:appDelegate.m_selectCharacterController animated:YES];
}

- (IBAction)alarmBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//	[self.navigationController pushViewController:appDelegate.m_addAlarmPageController animated:YES];
    [self presentViewController:appDelegate.m_addAlarmPageController animated:YES completion:nil];
}

- (IBAction)soundBtnClicked:(id)sender
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isEnableSound == YES) {
        appDelegate.isEnableSound = NO;
        [self.soundButton setBackgroundImage:[UIImage imageNamed:@"soundoff.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound Off";
        [appDelegate audioStop];
    }
    else
    {
        appDelegate.isEnableSound = YES;
        if (appDelegate.notificationFalg == YES) {
            if (appDelegate.notifSoundStr == nil || [appDelegate.notifSoundStr length] == 0 || appDelegate.notifSoundStr == @"") {
                NSLog(@"appDelegate.notifSoundStr = nil");
            }
            
            [appDelegate audioPlay:appDelegate.notifSoundStr isVibrate:YES];
        }
        
        [self.soundButton setBackgroundImage:[UIImage imageNamed:@"soundon.png"] forState:UIControlStateNormal];
        self.soundStatusLabel.text = @"Sound On";
//        [appDelegate audioPlay:YES isVibrate:YES];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.selectCharacterBtn release];
    [self.alarmBtn release];
    [self.soundButton release];
    [self.soundStatusLabel release];
    [self.CharacterImgView release];
    [self.characterNameLabel release];
    
    [super dealloc];
}

@end
