//
//  AddAlarmPageController.h
//  Alarm
//
//  Created by Billy on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAlarmPageController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton     *addAlarmBtn;
@property (strong, nonatomic) IBOutlet UITableView  *m_tableView;
@property (strong, nonatomic) IBOutlet UILabel      *timeLabel;
@property (strong, nonatomic) IBOutlet UIButton     *clockBtn;
@property (strong, nonatomic) NSTimer               *timer;
@property (strong, nonatomic) NSString              *timeStr;
@property (nonatomic) BOOL  isAddAlarmPage;

- (IBAction)addAddAlarmBtnClicked:(id)sender;
- (IBAction)clockBtnClicked:(id)sender;
- (void)changeStatus:(UISwitch *)sender;
- (void)showTime;

@end
